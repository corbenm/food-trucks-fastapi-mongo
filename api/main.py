from fastapi import FastAPI

from routers import trucks, users

app = FastAPI()

app.include_router(trucks.router)
# app.include_router(users.router)
