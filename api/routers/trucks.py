from fastapi import APIRouter, Depends, HTTPException
from queries.trucks import TruckQueries, TruckOut, TruckIn, TruckListOut

router = APIRouter()

@router.get("/api/trucks/{truck_id}", response_model=TruckOut)
def get_truck(
    truck_id: str,
    queries: TruckQueries = Depends(),
):
    record = queries.get_truck(truck_id)
    if record is None:
        raise HTTPException(status_code=404, detail="No truck found with id {}".format(truck_id))
    else:
        return record

@router.delete("/api/trucks/{truck_id}", response_model=bool)
def delete_truck(
    truck_id: str, 
    queries: TruckQueries = Depends()
):
    queries.delete_truck(truck_id)
    return True

@router.get("/api/trucks", response_model=TruckListOut)
def get_trucks(
    queries: TruckQueries = Depends()
):
    # returning a JSON object is conventional -- preferred instead of returning 
    # a list due to historical # security reasons and flexibility
    return {"trucks": queries.get_all_trucks()}

@router.post("/api/trucks", response_model=TruckOut)
def create_truck(
    truck: TruckIn,    
    queries: TruckQueries = Depends(),
):
    return queries.create_truck(truck)
